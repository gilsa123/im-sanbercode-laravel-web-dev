<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form bergabung</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
      @csrf
      <label for="first">First name:</label><br />
      <br />
      <input type="text" name="first" id="first" />
      <br />
      <br />
      <label for="last">Last name:</label><br />
      <br />
      <input type="text" name="last" id="last" />
      <br />
      <p>gender:</p>
      <input type="radio" name="gender" id="gender" value="Male" />
      <label for="gender">Male</label><br />
      <input type="radio" name="gender" id="gender" value="Female" />
      <label for="gender">Female</label><br />
      <input type="radio" name="gender" id="gender" value="Other" />
      <label for="gender">Other</label><br />
      <p>Nationality</p>
      <select name="national" id="national">
        <option value="indonesia">Indonesia</option>
        <option value="japan">Japan</option>
        <option value="singapura">Singapura</option>
        <option value="malaysian">Malaysian</option>
      </select>
      <br />
      <p>Language Spoken:</p>
      <input type="checkbox" name="lang1" id="lang1" value="bahasa" />
      <label for="lang1">Bahasa Indonesia</label><br />
      <input type="checkbox" name="lang2" id="lang2" value="english" />
      <label for="lang2">English</label><br />
      <input type="checkbox" name="lang3" id="lang3" value="other" />
      <label for="lang3">Other</label><br />
      <p>bio:</p>
      <br />
      <textarea name="bio" id="" cols="30" rows="10"></textarea><br />
      <input type="submit" name="kirim" id="kirim" value="Sign Up" />
    </form>
  </body>
</html>
