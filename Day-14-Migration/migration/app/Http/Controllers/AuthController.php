<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view("page.regis");
    }

    public function kirim(Request $request){
        $namaDepan = $request['first'];
        $namaBelakang = $request['last'];

        return view('page.welcome', ["namaDepan" => "$namaDepan", "namaBelakang" => "$namaBelakang"]);
    }
}
