<?php 
require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new Animal("Shaun");
echo "Domba Bernama: ". $sheep->name . "<br>";
echo "Punya ".$sheep->leg." Kaki<br>";
echo "Cold Blooded: ".$sheep->cold_blooded;
echo "<br>";
echo "<br>";

$sungokong = new Ape("Kera Sakti");
echo "Kera Bernama: ". $sungokong->name . "<br>";
echo "Punya ".$sungokong->leg." Kaki<br>";
echo "Cold Blooded: ".$sungokong->cold_blooded;
echo "<br>";
echo "Yell: ". $sungokong-> yell();
echo "<br>";
echo "<br>";

$kodok= new Frog("buduk");
echo "Kera Bernama: ". $kodok->name . "<br>";
echo "Punya ".$kodok->leg." Kaki<br>";
echo "Cold Blooded: ".$kodok->cold_blooded;
echo "<br>";
echo "Jump: ". $kodok-> jump();
echo "<br>";
?>